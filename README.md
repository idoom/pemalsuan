<p style="text-align: center"><img src="https://github.com/PemalsuanPHP/Artwork/raw/main/src/socialcard.png" alt="Social card of PemalsuanPHP"></p>

# Pemalsuan

[![Packagist Downloads](https://img.shields.io/packagist/dm/PemalsuanPHP/Pemalsuan)](https://packagist.org/packages/fakerphp/faker)
[![GitHub Workflow Status](https://img.shields.io/github/workflow/status/PemalsuanPHP/Pemalsuan/Tests/main)](https://github.com/PemalsuanPHP/Pemalsuan/actions)
[![Type Coverage](https://shepherd.dev/github/PemalsuanPHP/Pemalsuan/coverage.svg)](https://shepherd.dev/github/PemalsuanPHP/Pemalsuan)
[![Code Coverage](https://codecov.io/gh/PemalsuanPHP/Pemalsuan/branch/main/graph/badge.svg)](https://codecov.io/gh/PemalsuanPHP/Pemalsuan)

Pemalsuan is a PHP library that generates fake data for you. Whether you need to bootstrap your database, create good-looking XML documents, fill-in your persistence to stress test it, or anonymize data taken from a production service, Pemalsuan is for you.

It's heavily inspired by Perl's [Data::Pemalsuan](https://metacpan.org/pod/Data::Pemalsuan), and by Ruby's [Pemalsuan](https://rubygems.org/gems/faker).

## Getting Started

### Installation

Pemalsuan requires PHP >= 7.1.

```shell
composer require fakerphp/faker
```

### Documentation

Full documentation can be found over on [fakerphp.github.io](https://fakerphp.github.io).

### Basic Usage

Use `Pemalsuan\Factory::create()` to create and initialize a Pemalsuan generator, which can generate data by accessing methods named after the type of data you want.

```php
<?php
require_once 'vendor/autoload.php';

// use the factory to create a Pemalsuan\Generator instance
$faker = Pemalsuan\Factory::create();
// generate data by calling methods
echo $faker->name();
// 'Vince Sporer'
echo $faker->email();
// 'walter.sophia@hotmail.com'
echo $faker->text();
// 'Numquam ut mollitia at consequuntur inventore dolorem.'
```

Each call to `$faker->name()` yields a different (random) result. This is because Pemalsuan uses `__call()` magic, and forwards `Pemalsuan\Generator->$method()` calls to `Pemalsuan\Generator->format($method, $attributes)`.

```php
<?php
for ($i = 0; $i < 3; $i++) {
    echo $faker->name() . "\n";
}

// 'Cyrus Boyle'
// 'Alena Cummerata'
// 'Orlo Bergstrom'
```

## License

Pemalsuan is released under the MIT License. See [`LICENSE`](LICENSE) for details.

## Backward compatibility promise

Pemalsuan is using [Semver](https://semver.org/). This means that versions are tagged
with MAJOR.MINOR.PATCH. Only a new major version will be allowed to break backward
compatibility (BC).

Classes marked as `@experimental` or `@internal` are not included in our backward compatibility promise.
You are also not guaranteed that the value returned from a method is always the
same. You are guaranteed that the data type will not change.

PHP 8 introduced [named arguments](https://wiki.php.net/rfc/named_params), which
increased the cost and reduces flexibility for package maintainers. The names of the
arguments for methods in Pemalsuan is not included in our BC promise.
