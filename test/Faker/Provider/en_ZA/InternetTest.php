<?php

namespace Pemalsuan\Test\Provider\en_ZA;

use Pemalsuan\Provider\en_ZA\Company;
use Pemalsuan\Provider\en_ZA\Internet;
use Pemalsuan\Provider\en_ZA\Person;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class InternetTest extends TestCase
{
    public function testEmailIsValid()
    {
        $email = $this->faker->email();
        self::assertNotFalse(filter_var($email, FILTER_VALIDATE_EMAIL));
    }

    protected function getProviders(): iterable
    {
        yield new Person($this->faker);

        yield new Internet($this->faker);

        yield new Company($this->faker);
    }
}
