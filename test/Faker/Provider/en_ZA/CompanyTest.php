<?php

namespace Pemalsuan\Test\Provider\en_ZA;

use Pemalsuan\Provider\en_ZA\Company;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class CompanyTest extends TestCase
{
    public function testGenerateValidCompanyNumber()
    {
        $companyRegNo = $this->faker->companyNumber();

        self::assertEquals(14, strlen($companyRegNo));
        self::assertMatchesRegularExpression('#^\d{4}/\d{6}/\d{2}$#', $companyRegNo);
    }

    protected function getProviders(): iterable
    {
        yield new Company($this->faker);
    }
}
