<?php

namespace Pemalsuan\Test\Provider\es_VE;

use Pemalsuan\Provider\es_VE\Company;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class CompanyTest extends TestCase
{
    /**
     * national Id format validator
     */
    public function testNationalId()
    {
        $pattern = '/^[VJGECP]-?\d{8}-?\d$/';
        $rif = $this->faker->taxpayerIdentificationNumber;
        self::assertMatchesRegularExpression($pattern, $rif);

        $rif = $this->faker->taxpayerIdentificationNumber('-');
        self::assertMatchesRegularExpression($pattern, $rif);
    }

    protected function getProviders(): iterable
    {
        yield new Company($this->faker);
    }
}
