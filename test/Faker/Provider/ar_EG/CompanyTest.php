<?php

namespace Pemalsuan\Test\Provider\ar_EG;

use Pemalsuan\Calculator\Luhn;
use Pemalsuan\Provider\ar_EG\Company;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class CompanyTest extends TestCase
{
    public function testCompanyTaxIdNumberIsValid()
    {
        $companyTaxIdNumber = $this->faker->companyTaxIdNumber();
        self::assertMatchesRegularExpression('/\d{9}$/', $companyTaxIdNumber);
        self::assertTrue(Luhn::isValid($companyTaxIdNumber));
    }

    public function testCompanyTradeRegisterNumberIsValid()
    {
        $companyTradeRegisterNumber = $this->faker->companyTradeRegisterNumber();
        self::assertMatchesRegularExpression('/\d{6}$/', $companyTradeRegisterNumber);
        self::assertTrue(Luhn::isValid($companyTradeRegisterNumber));
    }

    protected function getProviders(): iterable
    {
        yield new Company($this->faker);
    }
}
