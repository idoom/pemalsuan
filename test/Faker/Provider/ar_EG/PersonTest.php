<?php

namespace Pemalsuan\Test\Provider\ar_EG;

use Pemalsuan\Calculator\Luhn;
use Pemalsuan\Provider\ar_EG\Person;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class PersonTest extends TestCase
{
    public function testNationalIdNumber()
    {
        $nationalIdNumber = $this->faker->nationalIdNumber();
        self::assertMatchesRegularExpression('/^2\d{13}$/', $nationalIdNumber);
        self::assertTrue(Luhn::isValid($nationalIdNumber));
    }

    protected function getProviders(): iterable
    {
        yield new Person($this->faker);
    }
}
