<?php

namespace Pemalsuan\Test\Provider\ar_EG;

use Pemalsuan\Provider\ar_EG\Text;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class TextTest extends TestCase
{
    public function testText()
    {
        self::assertNotSame('', $this->faker->realtext(200, 2));
    }

    protected function getProviders(): iterable
    {
        yield new Text($this->faker);
    }
}
