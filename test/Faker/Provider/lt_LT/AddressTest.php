<?php

namespace Pemalsuan\Test\Provider\lt_LT;

use Pemalsuan\Provider\lt_LT\Address;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class AddressTest extends TestCase
{
    public function testMunicipality()
    {
        self::assertStringEndsWith('savivaldybė', $this->faker->municipality());
    }

    protected function getProviders(): iterable
    {
        yield new Address($this->faker);
    }
}
