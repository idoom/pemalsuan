<?php

namespace Pemalsuan\Test\Provider\sv_SE;

use Pemalsuan\Provider\sv_SE\Municipality;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class MunicipalityTest extends TestCase
{
    public function testGenerate()
    {
        self::assertNotEmpty($this->faker->municipality());
    }

    protected function getProviders(): iterable
    {
        yield new Municipality($this->faker);
    }
}
