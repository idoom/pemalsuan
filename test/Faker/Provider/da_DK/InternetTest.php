<?php

namespace Pemalsuan\Test\Provider\da_DK;

use Pemalsuan\Provider\da_DK\Company;
use Pemalsuan\Provider\da_DK\Internet;
use Pemalsuan\Provider\da_DK\Person;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class InternetTest extends TestCase
{
    public function testEmailIsValid()
    {
        $email = $this->faker->email();
        self::assertNotFalse(filter_var($email, FILTER_VALIDATE_EMAIL));
    }

    protected function getProviders(): iterable
    {
        yield new Person($this->faker);

        yield new Internet($this->faker);

        yield new Company($this->faker);
    }
}
