<?php

namespace Pemalsuan\Test\Provider\kk_KZ;

use Pemalsuan\Provider\kk_KZ\Company;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class CompanyTest extends TestCase
{
    public function testBusinessIdentificationNumberIsValid()
    {
        $registrationDate = new \DateTime('now');
        $businessIdentificationNumber = $this->faker->businessIdentificationNumber($registrationDate);
        $registrationDateAsString = $registrationDate->format('ym');

        self::assertMatchesRegularExpression(
            '/^(' . $registrationDateAsString . ')([4-6]{1})([0-3]{1})(\\d{6})$/',
            $businessIdentificationNumber
        );
    }

    protected function getProviders(): iterable
    {
        yield new Company($this->faker);
    }
}
