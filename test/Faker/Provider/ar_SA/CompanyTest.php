<?php

namespace Pemalsuan\Test\Provider\ar_SA;

use Pemalsuan\Calculator\Luhn;
use Pemalsuan\Provider\ar_SA\Company;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class CompanyTest extends TestCase
{
    public function testCompanyIdNumberIsValid()
    {
        $companyIdNumber = $this->faker->companyIdNumber;
        self::assertMatchesRegularExpression('/^700\d{7}$/', $companyIdNumber);
        self::assertTrue(Luhn::isValid($companyIdNumber));
    }

    protected function getProviders(): iterable
    {
        yield new Company($this->faker);
    }
}
