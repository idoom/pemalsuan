<?php

namespace Pemalsuan\Test\Provider\it_CH;

use Pemalsuan\Calculator\Ean;
use Pemalsuan\Provider\it_CH\Person;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class PersonTest extends TestCase
{
    public function testAvs13Number()
    {
        $avs = $this->faker->avs13;
        self::assertMatchesRegularExpression('/^756\.([0-9]{4})\.([0-9]{4})\.([0-9]{2})$/', $avs);
        self::assertTrue(Ean::isValid(str_replace('.', '', $avs)));
    }

    protected function getProviders(): iterable
    {
        yield new Person($this->faker);
    }
}
