<?php

namespace Pemalsuan\Test\Provider\it_CH;

use Pemalsuan\Provider\it_CH\Company;
use Pemalsuan\Provider\it_CH\Internet;
use Pemalsuan\Provider\it_CH\Person;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class InternetTest extends TestCase
{
    public function testEmailIsValid()
    {
        $email = $this->faker->email();
        self::assertNotFalse(filter_var($email, FILTER_VALIDATE_EMAIL));
    }

    protected function getProviders(): iterable
    {
        yield new Person($this->faker);

        yield new Internet($this->faker);

        yield new Company($this->faker);
    }
}
