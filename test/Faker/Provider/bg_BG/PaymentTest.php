<?php

namespace Pemalsuan\Test\Provider\bg_BG;

use Pemalsuan\Provider\bg_BG\Payment;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class PaymentTest extends TestCase
{
    public function testVatIsValid()
    {
        $vat = $this->faker->vat();
        $unspacedVat = $this->faker->vat(false);
        self::assertMatchesRegularExpression('/^(BG \d{9,10})$/', $vat);
        self::assertMatchesRegularExpression('/^(BG\d{9,10})$/', $unspacedVat);
    }

    protected function getProviders(): iterable
    {
        yield new Payment($this->faker);
    }
}
