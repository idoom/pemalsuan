<?php

namespace Pemalsuan\Test\Provider\pl_PL;

use Pemalsuan\Provider\pl_PL\Address;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class AddressTest extends TestCase
{
    public function testState()
    {
        $state = $this->faker->state();
        self::assertNotEmpty($state);
        self::assertIsString($state);
        self::assertMatchesRegularExpression('/[a-z]+/', $state);
    }

    protected function getProviders(): iterable
    {
        yield new Address($this->faker);
    }
}
