<?php

namespace Pemalsuan\Test\Provider\de_AT;

use Pemalsuan\Provider\de_AT\Address;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class AddressTest extends TestCase
{
    /**
     * @see https://en.wikipedia.org/wiki/List_of_postal_codes_in_Austria
     */
    public function testPostcodeReturnsPostcodeThatMatchesAustrianFormat()
    {
        $postcode = $this->faker->postcode;

        self::assertMatchesRegularExpression('/^[1-9]\d{3}$/', $postcode);
    }

    protected function getProviders(): iterable
    {
        yield new Address($this->faker);
    }
}
