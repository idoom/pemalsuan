<?php

namespace Pemalsuan\Test\Provider\de_AT;

use Pemalsuan\Provider\de_AT\Company;
use Pemalsuan\Provider\de_AT\Internet;
use Pemalsuan\Provider\de_AT\Person;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class InternetTest extends TestCase
{
    public function testEmailIsValid()
    {
        $email = $this->faker->email();
        self::assertNotFalse(filter_var($email, FILTER_VALIDATE_EMAIL));
    }

    protected function getProviders(): iterable
    {
        yield new Person($this->faker);

        yield new Internet($this->faker);

        yield new Company($this->faker);
    }
}
