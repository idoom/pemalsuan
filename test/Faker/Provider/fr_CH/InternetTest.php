<?php

namespace Pemalsuan\Test\Provider\fr_CH;

use Pemalsuan\Provider\fr_CH\Company;
use Pemalsuan\Provider\fr_CH\Internet;
use Pemalsuan\Provider\fr_CH\Person;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class InternetTest extends TestCase
{
    public function testEmailIsValid()
    {
        $email = $this->faker->email();
        self::assertNotFalse(filter_var($email, FILTER_VALIDATE_EMAIL));
    }

    protected function getProviders(): iterable
    {
        yield new Person($this->faker);

        yield new Internet($this->faker);

        yield new Company($this->faker);
    }
}
