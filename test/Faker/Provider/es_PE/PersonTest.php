<?php

namespace Pemalsuan\Test\Provider\es_PE;

use Pemalsuan\Provider\es_PE\Person;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class PersonTest extends TestCase
{
    public function testDNI()
    {
        $dni = $this->faker->dni;
        self::assertMatchesRegularExpression('/\A[0-9]{8}\Z/', $dni);
    }

    protected function getProviders(): iterable
    {
        yield new Person($this->faker);
    }
}
