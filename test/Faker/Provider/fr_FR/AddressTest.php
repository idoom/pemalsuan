<?php

namespace Pemalsuan\Test\Provider\fr_FR;

use Pemalsuan\Provider\fr_FR\Address;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class AddressTest extends TestCase
{
    public function testSecondaryAddress()
    {
        $secondaryAdress = $this->faker->secondaryAddress();
        self::assertNotEmpty($secondaryAdress);
        self::assertIsString($secondaryAdress);
    }

    protected function getProviders(): iterable
    {
        yield new Address($this->faker);
    }
}
