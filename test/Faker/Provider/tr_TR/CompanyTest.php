<?php

namespace Pemalsuan\Test\Provider\tr_TR;

use Pemalsuan\Provider\tr_TR\Company;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class CompanyTest extends TestCase
{
    public function testCompany()
    {
        $company = $this->faker->companyField;
        self::assertNotNull($company);
    }

    protected function getProviders(): iterable
    {
        yield new Company($this->faker);
    }
}
