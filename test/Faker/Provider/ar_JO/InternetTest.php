<?php

namespace Pemalsuan\Test\Provider\ar_JO;

use Pemalsuan\Provider\fi_FI\Company;
use Pemalsuan\Provider\fi_FI\Internet;
use Pemalsuan\Provider\fi_FI\Person;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class InternetTest extends TestCase
{
    public function testEmailIsValid()
    {
        $email = $this->faker->email();
        self::assertNotFalse(filter_var($email, FILTER_VALIDATE_EMAIL));
    }

    protected function getProviders(): iterable
    {
        yield new Person($this->faker);

        yield new Internet($this->faker);

        yield new Company($this->faker);
    }
}
