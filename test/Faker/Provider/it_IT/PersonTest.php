<?php

namespace Pemalsuan\Test\Provider\it_IT;

use Pemalsuan\Provider\it_IT\Person;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class PersonTest extends TestCase
{
    public function testIfTaxIdCanReturnData()
    {
        $taxId = $this->faker->taxId();
        self::assertMatchesRegularExpression('/^[a-zA-Z]{6}[0-9]{2}[a-zA-Z][0-9]{2}[a-zA-Z][0-9]{3}[a-zA-Z]$/', $taxId);
    }

    protected function getProviders(): iterable
    {
        yield new Person($this->faker);
    }
}
