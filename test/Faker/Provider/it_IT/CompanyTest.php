<?php

namespace Pemalsuan\Test\Provider\it_IT;

use Pemalsuan\Provider\it_IT\Company;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class CompanyTest extends TestCase
{
    public function testIfTaxIdCanReturnData()
    {
        $vat = $this->faker->vat();
        self::assertMatchesRegularExpression('/^IT[0-9]{11}$/', $vat);
    }

    protected function getProviders(): iterable
    {
        yield new Company($this->faker);
    }
}
