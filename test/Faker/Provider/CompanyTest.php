<?php

namespace Pemalsuan\Test\Provider;

use Pemalsuan\Provider\Company;
use Pemalsuan\Provider\Lorem;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class CompanyTest extends TestCase
{
    public function testJobTitle()
    {
        $jobTitle = $this->faker->jobTitle();
        $pattern = '/^[A-Za-z]+$/';
        self::assertMatchesRegularExpression($pattern, $jobTitle);
    }

    protected function getProviders(): iterable
    {
        yield new Company($this->faker);

        yield new Lorem($this->faker);
    }
}
