<?php

namespace Pemalsuan\Test\Provider\zh_TW;

use Pemalsuan\Provider\zh_TW\Company;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class CompanyTest extends TestCase
{
    public function testVAT()
    {
        self::assertEquals(8, floor(log10($this->faker->VAT) + 1));
    }

    protected function getProviders(): iterable
    {
        yield new Company($this->faker);
    }
}
