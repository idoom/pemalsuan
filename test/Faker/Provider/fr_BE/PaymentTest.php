<?php

namespace Pemalsuan\Test\Provider\fr_BE;

use Pemalsuan\Provider\fr_BE\Payment;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class PaymentTest extends TestCase
{
    public function testVatIsValid()
    {
        $vat = $this->faker->vat();
        $unspacedVat = $this->faker->vat(false);
        self::assertMatchesRegularExpression('/^(BE 0\d{9})$/', $vat);
        self::assertMatchesRegularExpression('/^(BE0\d{9})$/', $unspacedVat);
    }

    protected function getProviders(): iterable
    {
        yield new Payment($this->faker);
    }
}
