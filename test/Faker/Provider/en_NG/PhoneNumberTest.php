<?php

namespace Pemalsuan\Test\Provider\en_NG;

use Pemalsuan\Provider\en_NG\PhoneNumber;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class PhoneNumberTest extends TestCase
{
    public function testPhoneNumberReturnsPhoneNumberWithOrWithoutCountryCode()
    {
        $phoneNumber = $this->faker->phoneNumber();

        self::assertNotEmpty($phoneNumber);
        self::assertIsString($phoneNumber);
        self::assertMatchesRegularExpression('/^(0|(\+234))\s?[789][01]\d\s?(\d{3}\s?\d{4})/', $phoneNumber);
    }

    protected function getProviders(): iterable
    {
        yield new PhoneNumber($this->faker);
    }
}
