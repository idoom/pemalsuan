<?php

namespace Pemalsuan\Test\Provider\en_NG;

use Pemalsuan\Provider\en_NG\Person;
use Pemalsuan\Test\TestCase;

/**
 * @group legacy
 */
final class PersonTest extends TestCase
{
    public function testPersonNameIsAValidString()
    {
        $name = $this->faker->name;

        self::assertNotEmpty($name);
        self::assertIsString($name);
    }

    protected function getProviders(): iterable
    {
        yield new Person($this->faker);
    }
}
