<?php

declare(strict_types=1);

namespace Pemalsuan\Extension;

/**
 * An extension is the only way to add new functionality to Pemalsuan.
 *
 * @experimental This interface is experimental and does not fall under our BC promise
 */
interface Extension
{
}
