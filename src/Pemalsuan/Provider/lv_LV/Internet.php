<?php

namespace Pemalsuan\Provider\lv_LV;

class Internet extends \Pemalsuan\Provider\Internet
{
    protected static $freeEmailDomain = ['mail.lv', 'apollo.lv', 'inbox.lv', 'gmail.com', 'yahoo.com', 'hotmail.com'];
    protected static $tld = ['com', 'com', 'net', 'org', 'lv', 'lv', 'lv', 'lv'];
}
