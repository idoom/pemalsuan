<?php

namespace Pemalsuan\Provider\lv_LV;

class PhoneNumber extends \Pemalsuan\Provider\PhoneNumber
{
    /**
     * {@link} https://en.wikipedia.org/wiki/Telephone_numbers_in_Latvia
     */
    protected static $formats = [
        '########',
        '## ### ###',
        '+371 ########',
    ];
}
