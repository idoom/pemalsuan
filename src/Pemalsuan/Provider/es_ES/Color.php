<?php

namespace Pemalsuan\Provider\es_ES;

class Color extends \Pemalsuan\Provider\Color
{
    protected static $safeColorNames = [
        'amarillo',
        'azul marino',
        'azul',
        'blanco',
        'celeste',
        'gris',
        'lima',
        'magenta',
        'marrón',
        'morado',
        'negro',
        'plata',
        'turquesa',
        'verde',
        'verde oliva',
    ];
}
