<?php

namespace Pemalsuan\Provider\pt_BR;

class Internet extends \Pemalsuan\Provider\Internet
{
    protected static $freeEmailDomain = ['gmail.com', 'yahoo.com', 'hotmail.com', 'uol.com.br', 'terra.com.br', 'ig.com.br', 'r7.com'];
    protected static $tld = ['com', 'com', 'com.br', 'com.br', 'net', 'net.br', 'br', 'org'];
}
