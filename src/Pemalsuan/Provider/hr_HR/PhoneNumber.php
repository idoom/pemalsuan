<?php

namespace Pemalsuan\Provider\hr_HR;

class PhoneNumber extends \Pemalsuan\Provider\PhoneNumber
{
    protected static $formats = [
        '+385 91 ### ####',
        '+385 92 ### ####',
        '+385 95 ### ####',
        '+385 98 ### ####',
        '+385 99 ### ####',
    ];
}
