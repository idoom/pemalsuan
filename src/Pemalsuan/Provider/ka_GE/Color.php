<?php

namespace Pemalsuan\Provider\ka_GE;

class Color extends \Pemalsuan\Provider\Color
{
    protected static $safeColorNames = [
        'ალისფერი', 'ვარდისფერი', 'თეთრი', 'იასამნისფერი', 'ლურჯი', 'მუქი ლურჯი', 'მწვანე', 'ნარინჯისფერი',
        'ნაცრისფერი', 'სალათისფერი', 'ღია მწვანე', 'ყავისფერი', 'ყვითელი', 'ცისფერი', 'წითელი',
    ];

    protected static $allColorNames = [
        'ალისფერი', 'ვარდისფერი', 'თეთრი', 'იასამნისფერი', 'ლურჯი', 'მუქი ლურჯი', 'მწვანე', 'ნარინჯისფერი',
        'ნაცრისფერი', 'სალათისფერი', 'ღია მწვანე', 'ყავისფერი', 'ყვითელი', 'ცისფერი', 'წითელი',
    ];
}
