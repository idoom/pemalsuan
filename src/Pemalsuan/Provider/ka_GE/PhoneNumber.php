<?php

namespace Pemalsuan\Provider\ka_GE;

class PhoneNumber extends \Pemalsuan\Provider\PhoneNumber
{
    protected static $formats = [
        '+995 ### ## ## ##',
        '### ## ## ##',
        '#########',
        '(###) ## ## ##',
        '+995(##)#######',
    ];
}
