<?php

namespace Pemalsuan\Provider\me_ME;

class PhoneNumber extends \Pemalsuan\Provider\PhoneNumber
{
    protected static $formats = [
        '+38220#####',
        '+38267#####',
        '+38269#####',
        '+382679#####',
        '+38268#####',
        '+38240#####',
    ];
}
