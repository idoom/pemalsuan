<?php

namespace Pemalsuan\Provider\is_IS;

class PhoneNumber extends \Pemalsuan\Provider\PhoneNumber
{
    /**
     * @var array Icelandic phone number formats.
     */
    protected static $formats = [
        '+354 ### ####',
        '+354 #######',
        '+354#######',
        '### ####',
        '#######',
    ];
}
