<?php

namespace Pemalsuan\Provider\fr_BE;

class Internet extends \Pemalsuan\Provider\Internet
{
    protected static $freeEmailDomain = ['gmail.com', 'hotmail.com', 'yahoo.com', 'advalvas.be'];
    protected static $tld = ['com', 'net', 'org', 'be'];
}
