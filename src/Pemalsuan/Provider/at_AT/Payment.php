<?php

namespace Pemalsuan\Provider\at_AT;

/**
 * @deprecated at_AT is not an existing locale, use {@link \Pemalsuan\Provider\de_AT\Payment}.
 * @see \Pemalsuan\Provider\de_AT\Payment
 */
class Payment extends \Pemalsuan\Provider\de_AT\Payment
{
}
