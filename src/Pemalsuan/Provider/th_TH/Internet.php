<?php

namespace Pemalsuan\Provider\th_TH;

class Internet extends \Pemalsuan\Provider\Internet
{
    protected static $tld = ['com', 'th', 'co.th', 'or.th', 'go.th', 'in.th', 'ac.th', 'mi.th', 'net.th'];
}
