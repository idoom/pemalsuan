<?php

namespace Pemalsuan\Provider\it_CH;

class Company extends \Pemalsuan\Provider\Company
{
    protected static $formats = [
        '{{lastName}} {{companySuffix}}',
        '{{lastName}} {{lastName}} {{companySuffix}}',
        '{{lastName}}',
        '{{lastName}}',
    ];

    protected static $companySuffix = ['SA', 'Sarl'];
}
