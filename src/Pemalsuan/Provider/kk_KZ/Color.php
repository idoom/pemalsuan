<?php

namespace Pemalsuan\Provider\kk_KZ;

class Color extends \Pemalsuan\Provider\Color
{
    protected static $safeColorNames = [
        'қара', 'қою қызыл', 'жасыл', 'қара көк', 'сарғыш түс',
        'күлгін', 'көк', 'көк', 'күміс',
        'сұр', 'сары', 'қызылкүрең түс', 'теңіз толқыны түс', 'ақ',
    ];
}
