<?php

namespace Pemalsuan\Provider\sk_SK;

class PhoneNumber extends \Pemalsuan\Provider\PhoneNumber
{
    protected static $formats = [
        '+421 ### ### ###',
        '00421 ### ### ###',
        '#### ### ###',
        '00421#########',
        '+421#########',
        '########',
    ];
}
