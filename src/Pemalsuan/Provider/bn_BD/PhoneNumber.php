<?php

namespace Pemalsuan\Provider\bn_BD;

class PhoneNumber extends \Pemalsuan\Provider\PhoneNumber
{
    public function phoneNumber()
    {
        $number = '+880';
        $number .= static::randomNumber(7);

        return Utils::getBanglaNumber($number);
    }
}
