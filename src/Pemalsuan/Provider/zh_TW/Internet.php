<?php

namespace Pemalsuan\Provider\zh_TW;

/**
 * @deprecated Use {@link \Pemalsuan\Provider\Internet} instead
 * @see \Pemalsuan\Provider\Internet
 */
class Internet extends \Pemalsuan\Provider\Internet
{
    /**
     * @deprecated Use {@link \Pemalsuan\Provider\Internet::userName()} instead
     * @see \Pemalsuan\Provider\Internet::userName()
     */
    public function userName()
    {
        return parent::userName();
    }

    /**
     * @deprecated Use {@link \Pemalsuan\Provider\Internet::domainWord()} instead
     * @see \Pemalsuan\Provider\Internet::domainWord()
     */
    public function domainWord()
    {
        return parent::domainWord();
    }
}
