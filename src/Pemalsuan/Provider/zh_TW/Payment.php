<?php

namespace Pemalsuan\Provider\zh_TW;

/**
 * @deprecated Use {@link \Pemalsuan\Provider\Payment} instead
 * @see \Pemalsuan\Provider\Payment
 */
class Payment extends \Pemalsuan\Provider\Payment
{
    /**
     * @return array
     *
     * @deprecated Use {@link \Pemalsuan\Provider\Payment::creditCardDetails()} instead
     * @see \Pemalsuan\Provider\Payment::creditCardDetails()
     */
    public function creditCardDetails($valid = true)
    {
        return parent::creditCardDetails($valid);
    }
}
