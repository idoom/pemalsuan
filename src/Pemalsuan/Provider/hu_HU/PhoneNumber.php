<?php

namespace Pemalsuan\Provider\hu_HU;

class PhoneNumber extends \Pemalsuan\Provider\PhoneNumber
{
    protected static $formats = [
        '+36-##-###-####',
        '+36#########',
        '+36(##)###-###',
        '06-##-###-####',
        '06(##)###-###',
    ];
}
