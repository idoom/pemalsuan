<?php

namespace Pemalsuan\Provider\hy_AM;

class Internet extends \Pemalsuan\Provider\Internet
{
    protected static $freeEmailDomain = ['gmail.com', 'yahoo.com', 'hotmail.com', 'yandex.ru', 'mail.ru', 'mail.am'];
    protected static $tld = ['com', 'com', 'am', 'am', 'am', 'net', 'org', 'ru', 'am', 'am', 'am'];
}
