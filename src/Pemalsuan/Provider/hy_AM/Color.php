<?php

namespace Pemalsuan\Provider\hy_AM;

class Color extends \Pemalsuan\Provider\Color
{
    protected static $safeColorNames = [
        'սև', 'դեղին', 'սպիտակ', 'մոխրագույն', 'կարմիր',
        'կապույտ', 'երկնագույն', 'կանաչ', 'կապտականաչ',
        'մանուշակագույն', 'շագանակագույն',
    ];
}
