<?php

namespace Pemalsuan\Provider\sl_SI;

class Internet extends \Pemalsuan\Provider\Internet
{
    protected static $freeEmailDomain = ['gmail.com', 'gmail.com', 'gmail.com', 'hotmail.com', 'yahoo.com', 'siol.net', 't-2.net'];

    protected static $tld = ['si', 'si', 'si', 'si', 'eu', 'com', 'info', 'net', 'org'];
}
