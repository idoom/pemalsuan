<?php

namespace Pemalsuan\Provider\en_CA;

class PhoneNumber extends \Pemalsuan\Provider\en_US\PhoneNumber
{
    protected static $formats = [
        '%##-###-####',
        '%##.###.####',
        '%## ### ####',
        '(%##) ###-####',
        '1-%##-###-####',
        '1 (%##) ###-####',
        '+1 (%##) ###-####',
        '%##-###-#### x###',
        '(%##) ###-#### x###',
    ];
}
