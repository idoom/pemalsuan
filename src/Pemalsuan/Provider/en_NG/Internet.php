<?php

namespace Pemalsuan\Provider\en_NG;

class Internet extends \Pemalsuan\Provider\Internet
{
    protected static $tld = ['com.ng', 'com', 'ng', 'net', 'edu.ng', 'org', 'gov.ng', 'org.ng', 'biz', 'co'];
}
